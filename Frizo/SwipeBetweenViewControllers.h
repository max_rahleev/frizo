//
//  RKSwipeBetweenViewControllers.h
//  RKSwipeBetweenViewControllers
//
//  Created by Richard Kim on 7/24/14.
//  Copyright (c) 2014 Richard Kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwipeBetweenViewControllers : UINavigationController <UIPageViewControllerDelegate, UIPageViewControllerDataSource>
@property (strong, nonatomic) NSMutableArray *viewControllersArray;
@end