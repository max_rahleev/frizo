//
//  RKSwipeBetweenViewControllers.m
//  RKSwipeBetweenViewControllers
//
//  Created by Richard Kim on 7/24/14.
//  Copyright (c) 2014 Richard Kim. All rights reserved.
//
//  @cwRichardKim for regular updates

#import "SwipeBetweenViewControllers.h"

@interface SwipeBetweenViewControllers ()
@property (assign, nonatomic) NSInteger currentPageIndex;
@property (strong, nonatomic) UIPageViewController *pageController;
@property (strong, nonatomic) UIPageControl *pageControl;
@end

@implementation SwipeBetweenViewControllers

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.viewControllersArray = [NSMutableArray array];
    self.currentPageIndex = 0;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self setupPageViewController];
}

- (void)setupPageViewController
{
    self.pageController = (UIPageViewController *)self.topViewController;
    self.pageController.delegate = self;
    self.pageController.dataSource = self;
    [self.pageController setViewControllers:@[[self.viewControllersArray objectAtIndex:0]]
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:YES
                                 completion:nil];
    
    self.pageControl = [[UIPageControl alloc]init];
    self.pageControl.numberOfPages = [self.viewControllersArray count];
    self.pageControl.currentPage = 0;
//    self.pageControl.enabled = NO;
    [self.pageControl addTarget:self action:@selector(actionPageControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    self.pageController.navigationController.navigationBar.topItem.titleView = self.pageControl;
}


#pragma mark - UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSInteger index = [self indexOfController:viewController];
    
    if (index == NSNotFound || index == 0) {
        return nil;
    }
    index--;
    
    return [self.viewControllersArray objectAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSInteger index = [self indexOfController:viewController];
    
    if (index == NSNotFound) {
        return nil;
    }
    index++;
    
    if (index == self.viewControllersArray.count) {
        return nil;
    }
    
    return [self.viewControllersArray objectAtIndex:index];
}


#pragma mark - UIPageViewControllerDelegate

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (completed) {
        self.currentPageIndex = [self indexOfController:[pageViewController.viewControllers lastObject]];
        self.pageControl.currentPage = self.currentPageIndex;
    }
}


#pragma mark - Helpers

- (NSInteger)indexOfController:(UIViewController *)viewController
{
    for (int i = 0; i < [self.viewControllersArray count]; i++) {
        if ([viewController isEqual:[self.viewControllersArray objectAtIndex:i]]) {
            return i;
        }
    }
    return NSNotFound;
}


#pragma mark - Actions

- (IBAction)actionPageControlValueChanged:(UIPageControl *)sender {
    
    UIViewController *vc = [self.viewControllersArray objectAtIndex:sender.currentPage];
    
    [self.pageController setViewControllers:@[vc] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

@end
